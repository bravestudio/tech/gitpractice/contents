# gitの練習。

apacheとnginxから同じcontentsを見れるようにしたい。

## 準備編

apacheとnginxでそれぞれ、最低限起動するだけの設定を作っておく。

* apache  
docker-composeで最低限apacheが動いて、contentsフォルダ下を参照するようにする。
```
shinjinakashima@shinjiNAKASHIMAsMBP gitpractice % cd apache 
shinjinakashima@shinjiNAKASHIMAsMBP apache % ll
total 8
drwxr-xr-x   5 shinjinakashima  staff  160  6 21 18:51 .
drwxr-xr-x   6 shinjinakashima  staff  192  6 21 17:55 ..
drwxr-xr-x  13 shinjinakashima  staff  416  6 21 18:56 .git
-rw-r--r--   1 shinjinakashima  staff  156  6 21 21:50 docker-compose.yml
shinjinakashima@shinjiNAKASHIMAsMBP apache % cat docker-compose.yml 
version: '3'
version: '3'
services:
  nginx:
    image: httpd:latest
    ports:
      - "8082:80"
    volume:
      - ./contents:/usr/local/apache2/htdocs
shinjinakashima@shinjiNAKASHIMAsMBP apache % 
```

* nginx  
apacheと同様に、dockerーcomposeで起動できるようにする。
```
shinjinakashima@shinjiNAKASHIMAsMBP gitpractice % cd nginx 
shinjinakashima@shinjiNAKASHIMAsMBP nginx % ll
total 8
drwxr-xr-x   5 shinjinakashima  staff  160  6 21 18:51 .
drwxr-xr-x   6 shinjinakashima  staff  192  6 21 17:55 ..
drwxr-xr-x  13 shinjinakashima  staff  416  6 21 18:54 .git
-rw-r--r--   1 shinjinakashima  staff  139  6 21 21:49 docker-compose.yml
shinjinakashima@shinjiNAKASHIMAsMBP nginx % cat docker-compose.yml 
version: '3'
services:
  nginx:
    image: nginx:latest
    ports:
      - "8081:80"
    volumes:
      - ./contents:/usr/share/nginx/html
shinjinakashima@shinjiNAKASHIMAsMBP nginx % 
```

* contentsフォルダを作成して中身を作る  
```
shinjinakashima@shinjiNAKASHIMAsMBP gitpractice % cd contents 
shinjinakashima@shinjiNAKASHIMAsMBP contents % ll
total 16
drwxr-xr-x   5 shinjinakashima  staff   160  6 21 22:05 .
drwxr-xr-x   5 shinjinakashima  staff   160  6 21 22:05 ..
drwxr-xr-x  14 shinjinakashima  staff   448  6 21 22:07 .git
-rw-r--r--   1 shinjinakashima  staff    73  6 21 18:07 index.html
-rw-r--r--   1 shinjinakashima  staff  1788  6 21 22:05 readme.md
shinjinakashima@shinjiNAKASHIMAsMBP contents % cat index.html 
<!DOCTYPE html>

<html>
<body>

<p>hello html world.</p>

</body>
<html>
shinjinakashima@shinjiNAKASHIMAsMBP contents % 
```

apacheとnginxとcontentsでそれぞれgitのリモートリポジトリを作っておく。
https://gitlab.com/bravestudio/tech/gitpractice


## つなげるのよ

それぞれ、apacheとnginxのフォルダで以下コマンドを実行する。
```
shinjinakashima@shinjiNAKASHIMAsMBP nginx % git submodule add https://gitlab.com/bravestudio/tech/gitpractice/contents.git
Cloning into '/Users/shinjinakashima/ws/gitpractice/nginx/contents'...
remote: Enumerating objects: 6, done.
remote: Counting objects: 100% (6/6), done.
remote: Compressing objects: 100% (5/5), done.
remote: Total 6 (delta 0), reused 0 (delta 0), pack-reused 0
Receiving objects: 100% (6/6), done.
shinjinakashima@shinjiNAKASHIMAsMBP nginx % 
```